import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAsync from 'vue-async-properties'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import createPersistedState from 'vuex-persistedstate'

import Explore from './Explore.vue'
import Home from './Home.vue'
import Login from './Login.vue'
import Register from './Register.vue'
import ProjectDetails from './ProjectDetails.vue'
import NewProject from './NewProject.vue'
import NewTask from './NewTask.vue'
import TaskDetails from './TaskDetails.vue'
import TestDetails from './TestDetails.vue'
import EditProject from './EditProject.vue'
import EditUser from './EditUser.vue'
import EditTask from './EditTask.vue'
import UserDetails from './UserDetails.vue'
import NotFound from './NotFound.vue'
import App from './Main.vue'

import './style/main.scss'

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(VueAsync)

const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    user: null,
    token: null
  },
  getters: {
    loggedIn: state => state.user !== null,
    user: state => state.user,
    token: state => state.token,
  },
  mutations: {
    setToken: (state, token) => {
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
      state.token = token

      window.setTimeout(async function () {
        const res = await axios.get('/api/v1/refresh')
        store.commit('setToken', res.data.token)
      }, 59 * 60 * 1000)
    },
    setUser: (state, user) => {
      state.user = user
    },
    logout: state => {
      state.user = null
    }
  }
})

axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.token}`

const router = new VueRouter({
  mode: 'history',
  routes:[
    { path: '/', component: Home, meta: { fullWidth: true } },
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    { path: '/explore', component: Explore },
    { path: '/settings', component: EditUser, meta: { requiresAuth: true } },
    { path: '/tasks/new/:project', component: NewTask, meta: { requiresAuth: true } },
    { path: '/projects/new', component: NewProject, meta: { requiresAuth: true } },
    { path: '/projects/:id', component: ProjectDetails },
    { path: '/projects/:id/edit', component: EditProject, meta: { requiresAuth: true } },
    { path: '/tasks/:id', component: TaskDetails },
    { path: '/tasks/:id/edit', component: EditTask, meta: { requiresAuth: true } },
    { path: '/tests/:id', component: TestDetails },
    { path: '/u/:username', component: UserDetails },
    { path: '*', component: NotFound },
  ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }

  if (to.matched.some(record => record.meta.private)) {
    if(!store.getters.loggedIn) {
      next()
    }
  }
})

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
})
