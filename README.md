# UserTest

A web application to make user testing easy for everyone.

User tests are a UX technique that is usually done in real-time, but this project
allows to do it asyncronously, allowing anyone to contribute when they have time.

## How does it work

Maintainers of projects can register and add their project to Usertest.
Then, they can define a list of tasks to do.

People who want to help can then choose a task and record themselves trying to do it,
while comenting everything they are doing.

The video is then saved and project maintainers can watch it whenever they want.
They can take notes about what was great, or on the contrary bad for the user experience
in their app. If they need clarification, they can send messages to the testers.

## How does it work (technically)

To allow recording someone's screen directly in the browser, we use WebRTC. On the server-side,
a WebRTC peer is started and connected to your computer when you record a test. The video is then
saved to be watched later.

## Contributing

You will need yarn, go and postgres.

Clone the repo, run `yarn` to install front-end dependencies,
create a postgres database, and then, run with:

```
yarn run parcel build index.html && go run main.go
```
